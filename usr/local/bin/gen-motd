#!/usr/bin/env bash

## Environment
NAME="${HOSTNAME}"
IPADDRESS=$(
  /sbin/ip a 2>&1 \
    | grep 'inet ' \
    | grep -v 'inet6 ' \
    | grep -v 'host lo' \
    | grep -v 'br-' \
    | grep -v 'docker' \
    | awk '{print $2}' \
    | cut -f 1 -d '/' \
    | sort -t . -k 3,3n -k 4,4n \
    | sed 'H;${x;s/\n/, /g;s/^,//;p;};d' \
    | xargs
  )

## Bash colours
RESET="\e[0m"
YELLOW="\e[33m"
RED="\e[31m"


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


## Is this a Raspberry Pi ?
if [ -e /proc/device-tree/model ]; then
  #DEVICE="$( cat /proc/device-tree/model )" ~ "bash: warning: command substitution: ignored null byte in input" for bash 4.4+
  DEVICE="$( tr -d '\0' < /proc/device-tree/model )"
## Method #1
elif [ -e /usr/sbin/dmidecode ] && [ -n "$( /usr/sbin/dmidecode -s system-manufacturer 2>/dev/null | xargs )" ]; then
  DEVICE=$(
    /usr/sbin/dmidecode -s system-manufacturer \
      | xargs \
      | tr '\n' ' ';
    /usr/sbin/dmidecode -s system-version \
      | xargs \
      | tr '\n' ' ';
    /usr/sbin/dmidecode -s system-product-name \
      | xargs
    )
## Method #2
elif [ -e /usr/sbin/dmidecode ] && [ -n "$( /usr/sbin/dmidecode -s baseboard-manufacturer 2>/dev/null | xargs )" ]; then
  DEVICE=$(
    /usr/sbin/dmidecode -s baseboard-manufacturer \
      | xargs \
      | tr '\n' ' ';
    /usr/sbin/dmidecode -s baseboard-product-name \
      | xargs
    )
fi

## Are we in a VM?
if command -v lscpu >/dev/null && \
  [ -n "$( lscpu | grep '^Hypervisor' )" ]; then
    DEVICE="${DEVICE} [ VM: $( lscpu | awk -F ':' '/Hypervisor/ {print $2}' | awk '{$1=$1};1' ) ]"
fi


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


## Get the hostname of the machine (via /etc/hostname)
if [ -e "/etc/hostname" ] && \
   [ "$( cat /etc/hostname )" != "${HOSTNAME}" ]; then
      NAME="${NAME} / $( cat /etc/hostname )"
fi

## Get the hostname of the machine (via hostname)
if command -v hostname >/dev/null; then
   if [ "$( hostname )" != "${HOSTNAME}" ] && \
      (
        [ -e "/etc/hostname" ] && \
        [ "$( hostname )" != "$( cat /etc/hostname )" ]
      ); then \
        NAME="${NAME} / $( hostname )"
   fi

   ## FQDN
   [ "$( hostname )" != "$( hostname -f )" ] && \
     NAME="${NAME} [ FQDN: $( hostname -f ) ]"
fi

## DHCP (Debian-baesd OSes)
find /var/lib/dhcp/ -name 'dhclient.*.leases' 2>/dev/null \
  | while read x; do
    y=$( awk -F '"' '/host-name/ {print $2}' ${x} | uniq )
    [ "${y}" != "${HOSTNAME}" ] && \
      (
        [ -e "/etc/hostname" ] && \
        [ "$( cat /etc/hostname )" != "${y}" ]
      ) && \
      (
        command -v hostname >/dev/null && \
        [ "$( hostname )" != "${y}" ]
      )

  NAME="${NAME} [ DHCP: ${y} ]"
done


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


if [ -e /etc/os-release ]; then
  OS="$( awk -F '"' '/^PRETTY_NAME=/ {print $2}' /etc/os-release | sed 's_GNU/Linux __' )"
  [ -e /etc/debian_version ] \
    && OS="${OS} - $( cat /etc/debian_version )"
  #source /etc/os-release
  #OS="${PRETTY_NAME}"
  #OS="$( awk -F '"' '/^NAME=/ {print $2}' /etc/os-release | tr '\n' ' ' ; awk -F '"' '/^VERSION=/ {print $2}' /etc/os-release )"
  #OS="$( awk -F '"' '/^NAME=/ {print $2}' /etc/os-release | tr '\n' ' '; awk -F '"' '/^VERSION_ID=/ {print $2}' /etc/os-release | tr '\n' ' '; awk -F '=' '/^VERSION_CODENAME=/ {print $2}' /etc/os-release )"
  #OS="$( awk -F '=' '/^ID=/ {print $2}' /etc/os-release )"
  ## Just get major version
  #VERSION="$( awk -F '"' '/VERSION_ID/ {print $2}' /etc/os-release | uniq )"
  #source /etc/os-release
  #VERSION="${VERSION_ID}"
elif [ -e /etc/debian_version ]; then
  OS="Debian $( cat /etc/debian_version )"
  [ -e /etc/os-release ] \
    && grep -q VERSION_CODENAME= /etc/os-release \
    && OS="${OS} ($( awk -F '=' '/^VERSION_CODENAME=/ {print $2}' /etc/os-release ))"
elif [ -e /etc/issue.net ]; then
  OS="$( cat /etc/issue.net )"
elif [ -e /etc/issue ]; then
  OS="$( cat /etc/issue )"
fi


## Check if Debian/Raspbian/Kali
if [ -e /etc/debian_version ] && \
  grep -qiE "^NAME=.*(Debian|Raspbian|Kali)" /etc/os-release; then
  [ -f /var/run/reboot-required ] \
    && OS="${OS} ${RED}*** REBOOT REQUIRED TO APPLY UPDATES ***${RESET}"
else
  OS="${RED}*** OS Isn't Debian-based *** Please Switch ***${RESET}"
fi


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


if command -v lscpu >/dev/null && \
  [ -n "$( lscpu | grep '^CPU(s)' )" ]; then
  #awk '$1 == "cpu" {
  #  printf "Used Cpu: %5.2f%%\n", 100*($2+$4)/($2+$4+$5)
  #  printf "Free Cpu: %5.2f%%\n", 100*$5/($2+$3+$4+$5)
  #}' /proc/stat
  CPU=$( cat <(grep 'cpu ' /proc/stat) <(sleep 1 && grep 'cpu ' /proc/stat) | awk -v RS="" '{print ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5) ""}' )
  CPU="CPUs: $( lscpu | awk -F ':' '/^CPU\(s\)/ {print $2}' | awk '{$1=$1};1' ) ($( printf "%.2f%%" ${CPU} ))"

  ## https://www.linuxatemyram.com/
  ## Newer than 2016+ (e.g. Debian 9+)
  RAM=""
  if command -v free >/dev/null; then
    RAM="$( free | awk '/Mem/ {print $3/$2 * 100.0}' )"
    RAM=" / RAM: $( free -h | awk '/Mem/ {print $2}' | sed -E 's/([0-9])([A-Z])/\1 \2/' )B ($( printf "%.2f%%" ${RAM} ))"
  fi
  HARDWARE="${CPU}${RAM}"
fi


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


rDNS=""
for x in $( echo ${IPADDRESS} | tr ',' '\n' ); do
  y=""

  if command -v getent >/dev/null; then
    y="$(
      getent hosts "${x}" \
        | awk '{print $2}'
      )"
  elif command -v dig >/dev/null; then
    y="$(
      dig +short -x "${x}" \
        | sed -E 's/\.$//'
      )"
  elif command -v nslookup >/dev/null; then
    y="$(
      nslookup "${x}" \
        | awk -F '=' '/name/ {print $2}' \
        | sed -E 's/^[[:space:]]*(.*)\.$/\1/'
      )"
  fi

  ## Didn't get anything? Skip
  [ -z "${y}" ] \
    && continue

  ## Is there already a value?
  [ -n "${rDNS}" ] \
    && rDNS="${rDNS},   "

  rDNS="${rDNS}${y}"
done


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


## 60 seconds * 60 minutes * 24 days = 86400
UPDAYS="$(( $( cat /proc/uptime | grep -o '^[0-9]\+' ) / 86400 ))"
#UPTIME=$( uptime -p | sed 's/^up //' )
UPTIME="${UPDAYS} days"

## Has it been MORE than 90 days (aka 91+)
[[ "${UPDAYS}" -gt 90 ]] \
  && UPTIME="${YELLOW}${UPTIME}${RESET}"


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


if command -v docker >/dev/null; then
  containers=$(
    docker info 2>/dev/null \
      | awk -F ': ' '/Running/ {print $2}'
    )

  stacks=$(
    docker ps \
        --quiet \
        --filter "label=com.docker.compose.project" \
      | xargs docker inspect \
        --format='{{index .Config.Labels "com.docker.compose.project"}}' \
        2>/dev/null \
      | sort -u \
      | wc -l
    )

    DOCKER="${containers} running / ${stacks} stacks"
fi


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


if command -v lxc-ls >/dev/null; then
  containers=$(
    /usr/bin/lxc-ls \
      | wc -w
    )

    LXC="${containers} running"
fi

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


## Might not be in $PATH for cron
if [ -e /usr/sbin/qm ]; then
  qmvm=$(
    /usr/sbin/qm list \
        | grep running \
        | wc -l
    )

    VM="${qmvm} running"
fi


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


## If they are not set, don't display
[ -z "${NAME}" ] \
  && NAME="${RED}---MISSING---${RESET}"

[ -z "${IPADDRESS}" ] \
  && IPADDRESS="${RED}---MISSING---${RESET}"

[ -z "${OS}" ] \
  && OS="${RED}---MISSING---${RESET}"

[ -z "${rDNS}" ] \
  && rDNS="${RED}---MISSING---${RESET}"

[ -z "${UPTIME}" ] \
  && UPTIME="${RED}---MISSING---${RESET}"

[ -z "${HARDWARE}" ] \
  && HARDWARE="${RED}---MISSING---${RESET}"

[ -z "${DEVICE}" ] \
  && DEVICE="${RED}---MISSING---${RESET}"


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


## Final output
echo -en """  Hostname: ${NAME}
  rDNS    : ${rDNS}
  IP      : ${IPADDRESS}
  OS      : ${OS}
  Uptime  : ${UPTIME}
  Device  : ${DEVICE}
  Hardware: ${HARDWARE}
"""


## Optional
[ -n "${DOCKER}" ] \
  && echo -e """  Docker  : ${DOCKER}"""

[ -n "${LXC}" ] \
  && echo -e """  LXC     : ${LXC}"""

[ -n "${VM}" ] \
  && echo -e """  VMs     : ${VM}"""
