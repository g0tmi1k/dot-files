## macOS Check list

- [ ] Wipe HDD
  - Format: `APFS`
  - Label: `macOS`

- - -

- [ ] Sign into Apple ID

- [ ] Apple App Store, download programs
  - `Bitwarden`
  - ~Kaleidoscope~ (non ARM64 support without using subscription)
  - `Microsoft Remote Desktop`
  - `Pixelmator Pro`
  - `Slack`
  - `WiregGuard`

- [ ] Download key programs outside Apple's App store
  - `Araxis Merge` - Check for product key
  - `BusyCal` - Check for product key (App store is subscription only)
  - `BusyContacts` - Check for product key (App store is subscription only)
  - `Discord`
  - `FireFox`
  - `Fork` - Check for product key
  - `GPG Suite` - Long term, nice to move away from (pref S/MINE, and the amount of background services aka "bloat")
  - `Homebrew`
  - `ImageOptim`
  - `iTerm`
  - `MailMate` - Check for product key
  - `Obsidian`
  - `Signal`
  - `Spotify`
  - `Sublime Text`
  - `Transmit 5` - Check for product key (App store is subscription only)
  - `Viscosity` - Check for product key
  - `Zoom`

- [ ] installed key programs via brew
  - `brew reinstall ansible   `
  - `brew reinstall arp-scan  ` - Optional
  - `brew reinstall binwalk   `
  - `brew reinstall diffoscope` - Optional
  - `brew reinstall dos2unix  ` - Optional
  - `brew reinstall exiftool  `
  - `brew reinstall grc       `
  - `brew reinstall html2text ` - Optional
  - `brew reinstall hugo      `
  - `brew reinstall jdupes    `
  - `brew reinstall jq        `
  - `brew reinstall nmap      `
  - `brew reinstall osxphotos `
  - `brew reinstall tree      ` - Optional
  - `brew reinstall yt-dlp    ` # youtube-dl: Upstream have not tagged a new release since 2021

- [ ] Sign into "home" Wi-Fi
  - On a device already connected, need to (temp) allow new connections to the AP
  - Make sure to disable it afterwards!

- [ ] Log into password manager

- [ ] Set OS settings (Apple -> System Settings)
  - If possible, open old laptop side by set and go though each part

- [ ] Set Finder settings (Finder -> Settings)
  - If possible, open old laptop side by set and go though each part

- [ ] Remove all OS widgets 
  - Click on the clock -> Scroll down -> Edit

- [ ] Program settings
  - Go into `/Application` and open each application that will be used

- [ ] Copy over SSH keys & configs (from `jumpbox.home`)
  - Home
  - Work
  - Git
  - External domains

- [ ] Copy over user home data (`~/`)

- [] Setup Time machine
  - Using SMB not AFP (Synology recommendation when using 11.0+/10.12+)

- [ ] Copy over GPG public/private keys
  - Replace `~/.gnupg` (not worried about `~/.gnupg_pre_2.1`)

- [ ] Copy over S/MINE public/private certificates
  - Keychain: `login` -> My certificates (shouldn't have a password)

- [ ] Import WireGuard VPN profiles
  - `~/Keys/vpn-wireguard/*.conf`

- [ ] Import OpenVPN profiles
  - `~/Keys/vpn-openvpn/*.visc`
