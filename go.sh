#!/usr/bin/env bash

## Quit on error
set -o errexit # set -e

## Don't use undefined variables
set -o nounset # set -u

## Quit at any point of the chain
set -o pipefail

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

## Git Repo
gitrepo="https://gitlab.com/g0tmi1k/dot-files/-/raw/main/home/user"


## What command to use to pull down
if command -v >&- wget; then
  cmd="wget --quiet --show-progres --continue --output-document=-"  #--no-verbose
elif command -v >&- curl; then
  cmd="curl --silent"
else
  echo "[-] Could not find wget/cURL" >&2
  exit 1
fi

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

## ZSH
echo "  [i] ${HOME}/.zshrc"
eval ${cmd} ${gitrepo}/.zshrc \
  > ${HOME}/.zshrc


## Bash
echo "  [i] ${HOME}/.bashrc"
eval ${cmd} ${gitrepo}/.bashrc \
  > ${HOME}/.bashrc


## Aliases
echo "  [i] ${HOME}/.aliases"
eval ${cmd} ${gitrepo}/.aliases \
  > ${HOME}/.aliases


## Tmux
echo "  [i] ${HOME}/.tmux.conf"
eval ${cmd} ${gitrepo}/.tmux.conf \
  > ${HOME}/.tmux.conf


## Message Of The Day (MOTD)
echo "  [i] /usr/local/bin/gen-motd"
eval ${cmd} ${gitrepo}/usr/local/bin/gen-motd \
  > /usr/local/bin/gen-motd
## Cron
cat <<EOF>/etc/cron.d/gen-motd
## Every hour & reboot
0 *   * * *   root   /usr/local/bin/gen-motd > /etc/motd
@reboot       root   /usr/local/bin/gen-motd > /etc/motd
EOF
## Permissions
chmod 0755 /usr/local/bin/gen-motd
## One-time run
/usr/local/bin/gen-motd \
  > /etc/motd


## Done
echo "  [i] Done"
exit
