## VM Settings

- Latest x64/AMD64 installer image
- Number of cores: `2`
- number of cores per processor: `2`
- Memory: `16384` MB (16GB) # This is for packaging SecLists
- Network: `Bridged`
- HDD: `200` GB
- Description
```plaintext
user: g0tmi1k / [...]

repo: nas.home

*Contains SSH & PGP/GPG keys*
```

## Installer Settings

- Hostname: `kali-dev`
- Username: `g0tmi1k`
- Metapackages: `Xfce` (disable all others)

## Post install

- Allow remote SSH login (just for this session)
  `sudo system start sshd`

- Disable notice of lack of tools
  `touch ~/.hushlogin`
-
- Add personal SSH key
  ```console
  mkdir -pv ~/.ssh/config.d/
  chmod 0700 ~/.ssh/
  cd ~/.ssh/
  echo "ssh-ed25519 [...]" > authorized_keys
  ```
- Break out of SSH and re-log in to test
- Disallow password login
  ```console
  sudo sed -i -E "s/^#?PasswordAuthentication .*/PasswordAuthentication no/" /etc/ssh/sshd_config
  sudo systemctl restart ssh
  sudo system --now enable ssh
  ```
- Update apt source to use local repo (nas)
```console
  cat <<EOF | sudo tee /etc/apt/sources.list
deb       http://nas.home/kali          kali-rolling  main contrib non-free non-free-firmware
#deb-src  http://nas.home/kali          kali-rolling  main contrib non-free non-free-firmware

#deb      http://repo.kali.org/kali     kali-rolling  main contrib non-free non-free-firmware
#deb      http://archive.kali.org/kali  kali-rolling  main contrib non-free non-free-firmware
#deb      http://http.kali.org/kali     kali-rolling  main contrib non-free non-free-firmware
#deb-src  http://http.kali.org/kali     kali-rolling  main contrib non-free non-free-firmware
EOF
  sudo apt update
```
- Upgrade packages/Kali
  ```console
  sudo apt -y dist-upgrade
  sudo apt -y autoremove --purge
  ```
- Install VIM
  `sudo apt -y install vim`
- Set default text editor
  `/usr/bin/select-editor` -> vim.tiny
- Install sublime 3 (stable)
  ```console
  wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
  echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
  sudo apt-get update
  sudo apt-get install sublime-text
  ```
- Upload SSH keys
```console
  scp *gitlab* g0tmi1k@192.168.1.110:/home/g0tmi1k/.ssh/
  scp *kali*   g0tmi1k@192.168.1.110:/home/g0tmi1k/.ssh/

  cd ~/.ssh/
  echo "Include config.d/*" > config
  vim config.d/git
  cat config.d/git
Host  gitlab.com
  IdentityFile  "${HOME}/.ssh/ed25519-256-gitlab.com"
  vim config.d/kali
  cat config.d/kali
Host  kali-repo repo.kali.org urania.kali.org 51.161.119.11
  IdentityFile  "${HOME}/.ssh/ed25519-256-OffSec"
  ````
- Test ssh to repo.kali.org
  `ssh repo.kali.org -l root -vv`
- Import GPG key
  ```console
  gpg --export-secret-keys g0tmi1k@kali.org > g0tmi1k@kali.org.asc
  scp g0tmi1k@kali.org.asc g0tmi1k@192.168.1.110:/home/g0tmi1k/
  shred -f --remove g0tmi1k@kali.org.asc

  gpg --import g0tmi1k@kali.org.asc
  gpg --fingerprint --with-colons g0tmi1k@kali.org | awk -F: '$1 == "fpr" {print $10":6:"}' | gpg --import-ownertrust
  gpg --list-keys
  shred -f --remove g0tmi1k@kali.org.asc
  ```
- Import dot files
  REF: https://gitlab.com/g0tmi1k/dot-files
  `echo "set mouse-=a" > ~/.vimrc`
- Clean up home folder
  ```console
  rm -rfv ~/{Documents,Music,Pictures,Public,Templates,Videos,.bashrc.original,.face.icon,.face}
  xdg-user-dirs-update
  cat ~/.config/user-dirs.dirs
  ```
- Add more wallpapers
  `sudo apt -y install kali-community-wallpapers`
- Increase /tmp size # This is for packaging SecLists
  ```console
  #sudo systemctl edit tmp.mount
  sudo mkdir -pv /etc/systemd/system/tmp.mount.d/
  cat <<EOF | sudo tee /etc/systemd/system/tmp.mount.d/override.conf
  [Mount]
  Options=mode=1777,strictatime,nosuid,nodev,size=50G,nr_inodes=1m
  EOF
  ```

## Xfce settings
### Personal
- Appearance -> Leave as is
- Clipboard Manager Settings -> History -> Disable
- Desktop
  Background -> Change wallpaper
  Icons -> Enable: Show hidden files on the desktop
- File Manager Preferences
  Display -> View Settings -> Enable: Remember vie settings for each folder
  Display -> Date -> Format: Today at HH:MM:SS
- Kali HiDPI Mode -> Leave as is
- Notification
  General -> Animations -> Enable: Slide out
- Panel
  Display -> Measurements -> Row size (pixels): 30
  Display -> Measurements -> Length (pixels): 30
  _May need to unlock and drag all the way to the top right, before re-locking_
- Panel Profiles
  Import
  ```console
  cli: xfce4-panel-profiles save xfce4-panels-kali-dev   # non tar.bz output
  gui: xfce -> settings -> panel profiles

  echo QlpoOTFBWSZTWZDvizEAATZ/4c6YABBC5///fGffCv/v//BAIAAGMAAICFAFXubq3DOnGNsZ3Zrq1CUTRMgTTJMNQnkaTINAZGTRkDT1AxPUaaBkJoECYiaaepBoAAAGgAAAACU9RIjQTUxHo1DE0AaaANANAAAADjI0yYmgyZMJpkDIaA0Bpk0MAJoDCRIKNU/SemknlPU9qZR6gAAANAAaaAA9TdxHDx6PsOiYPTkISHjQAa+DAtCDIIZsvHDlMcREIBuWyYHh49aduscySY5slWmUmqW6qiKpV1ppDFdU3lKbzE3STePMS1bNFmxEZ7dC6ZzU7eoqtsKCp/4yNWrFLiAupfn0s0JNMkMPZvgIVbKckvgvwtvmP27fGD6kUaxSKQxnEP96RhxTbWKdfTf+0sr8vVKvHz3XLwmHrrZONVUBfyT7vIAhv6DHhwLuqqoqIAiEkhUKBDsw4BoMopJxDf6rUms8xvtOkhGrq9M8rLa+9ux1g1boTkG2PtfriNOkQRURgGAOcXaOf4ggKs3dZap9V0Ab34hhSOWLspQQkkpySSTCbCbpCbrEKW0uEiTrJC17JoV+0BdMJCC5gkcfFxFBLeMo94Mo7ZQUBkKzul5EZ4y2EkkZrWGPhHSWYAmpsG0mMqsYRRAIxkWAJqGZiLQ0+Ob+RM4sKC1+26gLcG4yHUUUTUXToChtGFRikpxFbZQ1Yi7KuO4aoihyrkTdhVKyWFnogkKywU74nMc1hEuncSMZbB+8oZDESsmMg8QuoZtbOaakxeoRhw3yEoF7IBQyT8UW3CyYxdb6iVgaJUMBFLgloiFwhClKiAcSiJDYhQXuTmMLr9SZijCtqvVWj5ZkUQVGIRWBYAUXg2jf4uDh3eFvvutaov7+bXh0clnYbFTIalVmhlrbtsYtMtGGMgesfPdSpiVziHG0b7J6DnRQb7njbIicYiY+JeSmrz7ZyszRqDVnzmgjy2cVSUkYrSv7XSjvZ+gOH1Jgj0JQpyOQTVDDUD8FntfcanFNXOWlWObXVin9/UKSlmOBNYemTTU7rOKVqISG2Bwuz4VJCnc68SEC9lRTJNmKQCMgRFQC5GTcK3mAfw3ngpTfoLnUGFYFWtnDO0fvVddyG29NjFdV1rU30N2F/gHExULwFTqJORNyYTQznKI8y23EsBYhXalrtsyml11aWqXCql1HZdZ5dpkevXebGvBnEnH+KaRe9EVXDX2GdGtAAFB7qXIkYwuMmy6kXLmROka+fLpdtNOba0QE5YaHc0CLCAAJ41GGpXJJOYMazBW7hUYbJlt7WaQ6F9jmIYIgWSq2DNSqZAoQkr05kLucnLD2ZytwtylZ4ioqAWgZGFgABKyudXxdiMETyKjgcsUThrxuE3QuKAA0zK/csZCjDJWhQRh0CEFuLn2rYKCqVYJioYkkpAKDRLWIXXtO8h1ngbaekUMgUB4Z30ChEQScuTyYbxavI0O3aQNHJupSmfVpwlG3Whaa/qRhK3TMQAY6aItsJgy1tsQBEQ7c83boqHKJdZblc5Mc+aGBuItIFImCWgmA2JzbWbcZ2UdfGJASCIiJAZ6mu3REC9nZscX4QTAqdmLsXSBiAraBF3BFppOmO1LILZVthgDCaRMwPWdbqAdCksiNkyVk1qmGwjTmHCxLNh8CODcQNQRPEVM0rFhOuYP5XEhSmclIANmQV8b4HBfEYu0G7TvV5FLc2xPTc1AKqF8O5sCLaR/qoRlMqtXXNCmerLg3Hg3DMnogEf8XckU4UJCQ74sx | base64 -d > xfce4-panel-profile-export-kali-dev.tar.bz
  md5sum xfce4-panel-profile-export-kali-dev.tar.bz # 868408fe8c7e5bb5853bcc5ed725ab3f  xfce4-panel-profile-export-kali-dev.tar.bz
```
- Qt5 Settings -> Leave as is
- Qt6 Settings -> Leave as is
- Text Editor Settings -> Leave as is
- Window Manager
  Style -> Title alignment: Center
  Style -> Button layout -> "Close", "Maximize", "Minimize", "Menu", "Title"
- Windows Manager Tweaks
  Accessibility -> Disable: Hide frame of windows when maximized
- Workspaces
  General -> Number of workspaces: 2
### Hardware
- Power Manager
  Display -> Blank after: never
### System
- LightDM GTK+ Greeter settings
  Appearance -> Disable: User image
- Session and Startup
  General -> Logout Settings -> Disable: Prompt on logout

- Xfce menu
  _aka Whisker Menu (Kali icon, top right)_
  Favorites (Disable everything else) -> Terminal Enumerator, File Manager, Firefox ESR, Sublime Text
- qTerminal (Open -> Right click -> Preferences)
  Appearance -> Disable: Show the menu bar
  Appearance -> Enable: Fixed tag width -> 250px
  Appearance -> Start with preset -> 2 terminals horizontally
  Appearance -> Terminal margin: 10px
  Behavior -> Action after paste: No move
  Behavior -> Enable: Ask for confirmation when closing
  Behavior -> Disable: Open new terminal in current working directory
- File Manager
  _aka Thunar_
  Edit -> Configure custom actions (Disable everything else) -> Open Terminal Here
  View -> Enable: Show Hidden Files
  Shortcuts
```console
  cat <<EOF> ~/.config/gtk-3.0/bookmarks
file:///home/g0tmi1k/Downloads
file:///home/g0tmi1k/kali
file:///home/g0tmi1k/kali/packages
file:///home/g0tmi1k/kali/build-area
EOF
```
